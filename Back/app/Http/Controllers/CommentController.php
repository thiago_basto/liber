<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function create(Request $request){
        $comment = new Comment;
        $comment->title_text = $request->title_text;
        $comment->text = $request->text;
        $comment->user_id = $request->user_id;
        $comment->book_id = $request->book_id;
        $comment->save();
        return response()->json(['comment' => $comment],200);
    }

    public function index() {
        $comments = Comment::all();
        return response()->json(['comment' => $comments],200);

    }

    public function show($id) {
        $comment = Comment::find($id);
        return response()->json(['comment' => $comment],200);
    }

    public function update(Request $request, $id) {
        $comment = Comment::find($id);

        if($request->title_text) {
            $comment->title_text = $request->title_text;
        }

        if($request->text) {
            $comment->text = $request->text;
        }
        $comment->save();
        return response()->json(['comment' => $comment],200);
    }

    public function destroy($id) {
        $comment = Comment::find($id);
        $comment->delete();
        return response()->json(['Comentario excluido com sucesso'],200);
    }
}
