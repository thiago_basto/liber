<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Book extends Model
{
    use HasFactory;

    public function create(Request $request){
        $this->title = $request->title;
        $this->author = $request->author;
        $this->publisher = $request->publisher;
        $this->category = $request->category;
        $this->condition = $request->condition;
        $this->price = $request->price;
        $this->save();
    }

    public function updateBook(Request $request, $id){
        if ($request->title){
            $this->title = $request->title;
        } 

        if ($request->author){
            $this->author = $request->author;
        }

        if ($request->publisher){
            $this->publisher = $request->publisher;
        }

        if ($request->category){
            $this->category = $request->category;
        }

        if ($request->condition){
            $this->condition = $request->condition;
        }

        if ($request->price){
            $this->price = $request->price;
        }

        $this->save();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function comment(){
        return $this->belongsTo('App\Models\Comment');
    }
}
