<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public function create(Request $request){
        $this->title_text = $request->title_text;
        $this->text = $request->text;
        $this->book_id = $request->book_id;
        $this->user_id = $request->user_id;
        $this->save();
    }

    public function updateComment(Request $request, $id){
        if($request->title_text){
            $this->title_text = $request->title_text;
        }

        if($request->text){
            $this->text = $request->text;
        }
        $this->save();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    
    public function books(){
        return $this->belongsTo('App\Models\Book');
    }
    

}
