import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AboutBookPage } from './about-book.page';

describe('AboutBookPage', () => {
  let component: AboutBookPage;
  let fixture: ComponentFixture<AboutBookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutBookPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AboutBookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
