import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

class Livros {
  id: number;
  capa: string;
  titulo: string; 
  escritor: string;
  preço: number;
  quantidade: number;
  estadoDoLivro: string;
}

@Component({
  selector: 'app-about-book',
  templateUrl: './about-book.page.html',
  styleUrls: ['./about-book.page.scss'],
})
export class AboutBookPage implements 

OnInit {

  constructor(public toastController: ToastController){ }
  
  async openToastBuy() {
    const toast = await this.toastController.create({
      message: 'O livro foi adicionado ao carrinho',
      duration:500,
      color: 'dark',
    });
    toast.present();
  }

    async openToastFavorite() {
      const toast = await this.toastController.create({
        message: 'O livro foi favoritado',
        duration:500,
        color: 'dark',
      });
      toast.present();
    }



  favoriteIcon: string = "heart-outline";

  livros:Livros[];

  favoritos: string[];


  buyAction(){
    this.openToastBuy()
    console.log('voce comprou')
  }

  favoriteAction(Livro: string): string{
    if(this.favoriteIcon === "heart-outline"){
      this.favoriteIcon = "heart";
      this.openToastFavorite()
      console.log('você favoritou o livro!')
    } else if(this.favoriteIcon === "heart"){
        this.favoriteIcon = "heart-outline"
      }
    return 'Livro adicionado com sucesso';
  }


  ngOnInit() {

    this.livros = [ 
      {
        id: 1,
        capa: '../assets/images/forrest_gump.png',
        titulo: 'Forrest Gump',
        escritor: 'Winstom Groom',
        preço: 100.00,
        quantidade: 497,
        estadoDoLivro: 'Novo'
      },

      {
        id: 2,
        capa: '../assets/images/jogador.png',
        titulo: 'Jogador N1',
        escritor: 'Ernest Cline',
        preço: 50.00,
        quantidade: 32,
        estadoDoLivro: 'Usado'
      },

      {
        id: 3,
        capa: '../assets/images/estrelas.png',
        titulo: 'A Culpa é das estrelas',
        escritor: 'John Green',
        preço: 39.99,
        quantidade: 10,
        estadoDoLivro: 'Usado'
      },

      {
        id: 4,
        capa: '../assets/images/it.png',
        titulo: 'It, A coisa',
        escritor: 'Stephen King',
        preço: 20.00,
        quantidade: 3,
        estadoDoLivro: 'Novo'
      },

      {
        id: 5,
        capa: '../assets/images/jogos_vorazes.png',
        titulo: 'Jogos Vorazes',
        escritor: 'Suzanne Collins',
        preço: 19.99,
        quantidade: 64,
        estadoDoLivro: 'Novo'
      },
    ]
  }
}
