import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AboutBookPageRoutingModule } from './about-book-routing.module';

import { AboutBookPage } from './about-book.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AboutBookPageRoutingModule
  ],
  declarations: [AboutBookPage]
})
export class AboutBookPageModule {}
